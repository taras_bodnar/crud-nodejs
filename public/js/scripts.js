
$(document).ready(function(){/* google maps -----------------------------------------------------*/
google.maps.event.addDomListener(window, 'load', initialize);

function initialize() {
  var styles = [
    {
      "featureType": "administrative",
      "elementType": "labels.text.fill",
      "stylers": [
        {
          "color": "#444444"
        }
      ]
    },
    {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [
        {
          "color": "#f2f2f2"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
        {
          "saturation": -100
        },
        {
          "lightness": 45
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "simplified"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "geometry",
      "stylers": [
        {
          "visibility": "simplified"
        },
        {
          "color": "#ff6a6a"
        },
        {
          "lightness": "0"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.text",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road.highway",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#ff6a6a"
        },
        {
          "lightness": "75"
        }
      ]
    },
    {
      "featureType": "road.arterial",
      "elementType": "labels.icon",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road.local",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "lightness": "75"
        }
      ]
    },
    {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "transit.line",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "transit.station.bus",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "transit.station.rail",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "on"
        }
      ]
    },
    {
      "featureType": "transit.station.rail",
      "elementType": "labels.icon",
      "stylers": [
        {
          "weight": "0.01"
        },
        {
          "hue": "#ff0028"
        },
        {
          "lightness": "0"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "on"
        },
        {
          "color": "#80e4d8"
        },
        {
          "lightness": "25"
        },
        {
          "saturation": "-23"
        }
      ]
    }
  ];
  if (map_data.length) {
//                console.log(map_data[0].lat);
    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
    var bounds = new google.maps.LatLngBounds();

    var clusterStyles = [{
      textColor: 'white',
      fontSize: '10',
      url: '/public/img/pin.png',
      backgroundPosition: 'bottom',
      backgroundRepeat: 'no-repeat',
      height: 46,
      width: 46
    }];

    var latlng = new google.maps.LatLng(map_data[0].lat, map_data[0].lng);
    var myOptions = {
      zoom: 7,
      disableDefaultUI: true,
      center: latlng,
      scrollwheel: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
      },
    };

    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
    var marker, i, ib = [];
    var markerCluster = new MarkerClusterer(map, marker, {
      styles: clusterStyles
    });
    map_data.forEach(function (item, i) {
      //console.log(item);
      //var gps = item.gps.split(',');
      var c1 = parseFloat(item.lat), c2 = parseFloat(item.lng);

      var pos = new google.maps.LatLng(c1, c2);
      bounds.extend(pos);


      marker = new google.maps.Marker({
        title: item.name+","+item.name,
        position: pos,
        map: map
      });
      //  console.log("/uploads/mapicon/"+item.icon);
      //ib.i = item.name;

      google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
          // console.log(ib.i, marker, i, item.name);
          if (ib.i) {
            ib.i.close();
          }

          ib.i = new google.maps.InfoWindow({
            content: "<a target='_blank' href='institution/"+item.id+"'>"+item.name+"</a><br>"+item.address+"<br>"+item.phone
          });

          ib.i.open(map, marker);
          //setTimeout(function () {ib.i.close(); }, 5000);
        }
      })(marker, i));
      markerCluster.addMarker(marker);
      map.fitBounds(bounds);
      google.maps.event.addListener(map, 'click', function () {

        for (var i = 0; i < ib.length; i++) {
          ib[i].close();
        }
      });
    });
  }
  map.addListener('zoom_changed', function() {
    console.log(map.getZoom());
  });
};


/* end google maps -----------------------------------------------------*/
});