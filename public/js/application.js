function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
if ($('.importMap').length) {
    /*
     KEY
     AIzaSyAoaSdbHeEOitKQNcGoGAXFHYYYTCAWLGM - http
     AIzaSyBGbDYyyZvmx2bl8rDIRd7OFoiraf0WCnQ - js

     RESOURCES
     https://developers.google.com/places/place-id
     https://developers.google.com/places/supported_types
     https://developers.google.com/maps/articles/phpsqlsearch_v3#populating-the-table
     https://developers.google.com/maps/articles/phpsqlajax_v3
     https://developers.google.com/maps/documentation/javascript/examples/place-radar-search


     !!! ВАЖЛИВІ

     https://developers.google.com/places/web-service/search

     ЗАМІТКИ

     ІД обєкта
     ...
     "place_id" : "ChIJrTLr-GyuEmsRBfy61i59si0",
     ...

     КРаЙНЯ ЛІВА
     48.9149194,-125.4281465,9.25z


     */

    function gMapCrawler(y0, y1, x0, x1)
    {
        var i = 0, step = 0.01,  defs = [], points = [];
        if(i == 0){
            defs = [y0, y1, x0, x1]
        }

        function crawl(y0, y1, x0, x1)
        {
            while(y0 > y1){
                var point = { lat:y0, lng: x0 };
//            console.log(y0, y1, ' P: ', point);
                points.push(point);
                y0 -= step;
            }

            if( y0 < y1 && x0 < x1 ){
                y0 = defs[0]; x0 += step;
                crawl(y0, y1, x0, x1)
            }
        }

        crawl(y0, y1, x0, x1);

        return points;
    }

    var map,
        r= 0,
        points = [],
        start,
        startFrom = 0,
        endAt = false;

    var LAT = 0, LNG = 0, RAD = 50000, ZOOM = 10, TYPES = [
            'accounting',
            'airport',
            'amusement_park',
            'aquarium',
            'art_gallery',
            'atm',
            'bakery',
            'bank',
            'bar',
            'beauty_salon',
            'bicycle_store',
            'book_store',
            'bowling_alley',
            'bus_station',
            'cafe',
            'campground',
            'car_dealer',
            'car_rental',
            'car_repair',
            'car_wash',
            'casino',
            'cemetery',
            'church',
            'city_hall',
            'clothing_store',
            'convenience_store',
            'courthouse',
            'dentist',
            'department_store',
            'doctor',
            'electrician',
            'electronics_store',
            'embassy',
            'fire_station',
            'florist',
            'funeral_home',
            'furniture_store',
            'gas_station',
            'grocery_or_supermarket',
            'gym',
            'hair_care',
            'hardware_store',
            'hindu_temple',
            'home_goods_store',
            'hospital',
            'insurance_agency',
            'jewelry_store',
            'laundry',
            'lawyer',
            'library',
            'liquor_store',
            'local_government_office',
            'locksmith',
            'lodging',
            'meal_delivery',
            'meal_takeaway',
            'mosque',
            'movie_rental',
            'movie_theater',
            'moving_company',
            'museum',
            'night_club',
            'painter',
            'park',
            'parking',
            'pet_store',
            'pharmacy',
            'physiotherapist',
            'plumber',
            'police',
            'post_office',
            'real_estate_agency',
            'restaurant',
            'roofing_contractor',
            'rv_park',
            'school',
            'shoe_store',
            'shopping_mall',
            'spa',
            'stadium',
            'storage',
            'store',
            'subway_station',
            'synagogue',
            'taxi_stand',
            'train_station',
            'transit_station',
            'travel_agency',
            'university',
            'veterinary_care',
            'zoo'
        ],
        PYRMONT = {lat:LAT, lng: LNG};

    function runInit(a, b)
    {
        startFrom = a;
        endAt = b;
        start = true;
        initMap();
    }

    function initMap() {
        if (! start) {
            console.log('Ready ...');
            return;
        }
            console.log('initMap ' , r);
            // crawl points
            if(r == 0){
                //points = gMapCrawler(49.016816, 24.633685, -125.451065, -67.816487);
                points = gMapCrawler(42.046609, 40.522540, -74.980628, -71.678444);
                console.log(points.length, 'points to analyze');
            } else if (r == 1) {
                r = startFrom;
            }

            if(typeof points[r] == 'undefined'){
                console.log('Done');
                return;
            }
            if (endAt && r == endAt) {
                console.log('Out of limit. Done');
                return;
            }

            LAT = points[r].lat; LNG = points[r].lng; PYRMONT = {lat:LAT, lng: LNG};

    //            if(r == 0){
            map = new google.maps.Map(document.getElementById('map'), {
                center: PYRMONT,
                zoom: ZOOM
            });
    //            }

            runService(map);
    }


    //var map;
    //var infowindow;
    var importUrl = $('input[name="importUrl"]').val();

    function runService(map)
    {
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
            location: PYRMONT,
            radius: RAD,
            types: TYPES,
            language: 'en'
        }, callback);
    }

    function loadInstitution(results, i)
    {
        //if (results[i].opening_hours && results[i].opening_hours.open_now) {

            var service = new google.maps.places.PlacesService(map);
            service.getDetails({
                placeId: results[i].place_id
            }, function(result, status) {
                if (status == 'OK') {
                    $.ajax({
                        url: importUrl,
                        type: 'post',
                        data: {
                            name: result.name,
                            categories: result.types,
                            address: result.formatted_address, // The formatted_address property is only returned for a Text Search
                            phone: (result.international_phone_number) ? result.international_phone_number : '',
                            site: (result.website) ? result.website : '',
                            lat: result.geometry.location.lat(),
                            lng: result.geometry.location.lng(),
                            service_id: result.place_id,
                            all_day: (result.opening_hours && result.opening_hours.open_now && result.opening_hours.periods && (!result.opening_hours.periods.close || result.opening_hours.periods.open.day==0 || result.opening_hours.periods.open.time=="0000")) ? 1 : 0,
                            working_hours: (result.opening_hours) ? JSON.stringify(result.opening_hours) : ''
                        },
                        success: function () {
                            $('.objectsCount').text($('.objectsCount').text()*1 + 1);
                        }


                    }).done(function () {
                        if (i < (results.length - 1) ) {
                            setTimeout(function () {
                                loadInstitution(results, ++i);
                            }, 100);
                        }
                    });
                    console.log({
                        name: result.name,
                        categories: result.types,
                        address: result.formatted_address, // The formatted_address property is only returned for a Text Search
                        phone: result.international_phone_number,
                        site: result.website,
                        lat: result.geometry.location.lat(),
                        lng: result.geometry.location.lng(),
                        service_id: result.place_id,
                        all_day: (result.opening_hours && result.opening_hours.periods && (!result.opening_hours.periods.close || result.opening_hours.periods.open.day==0 || result.opening_hours.periods.open.time=="0000")) ? 1 : 0,
                        working_hours: (result.opening_hours) ? JSON.stringify(result.opening_hours) : ''
                    });
                }

            });

            //console.log(results[i]);

        //} else {
        //    if (i < (results.length - 1) ) {
        //        setTimeout(function () {
        //            loadInstitution(results, ++i);
        //        }, 100);
        //    }
        //}
    }

    function callback(results, status, pagination) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            loadInstitution(results, 0);

            if (pagination.hasNextPage) {
                setTimeout(function(){
                    console.log('call next page');
                    pagination.nextPage();
                }, 500);
            } else {

                setTimeout(function(){
                    r++;
                    initMap();
                }, 500);

            }
        } else if (status === google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {

            console.log('ZERO_RESULTS, continue ...');

            setTimeout(function(){
                r++;
                initMap();
            }, 500);
        } else if (status == 'OVER_QUERY_LIMIT') {
            var getStart = (r > 0) ? r : ((parseInt(getParameterByName('from')) > 0) ? parseInt(getParameterByName('from')) : startFrom + 1);
            var getEnd = endAt;
            console.log(status);
            location.href = location.origin + location.pathname + '?from='+getStart+'&to='+getEnd;
        } else {
            console.log(status);
            setTimeout(function(){
                r++;
                initMap();
            }, 500);
        }
    }
    $('#startImport').on('click', function () {
        var from = parseInt($('input[name="from"]').val());
        var to = parseInt($('input[name="to"]').val());
        if (from > 0 && to > 0 && to > from) {
            $('.objectsCount').parent('p').show();
            $('.objectsCount').parent('p')[0].childNodes[0].nodeValue = ' institution analyzed';
            $('.objectsCount').text(0);
            runInit(from, to);
        } else {
            alert('Invalid start parameters');
        }
    });
    $('#clearCookies').on('click', function () {
        console.log('clearing cookies');
        location.href = location.origin + location.pathname;
        return false;
    });

}

if ($('.updateMap').length) {
    function createPhotoMarker(place) {
        var photos = place.photos;
        if (!photos) {
            return;
        }

        var photosArr = [];
        for(var i=0;i<=photos.length-1;i++) {
            photosArr[i] = photos[i].getUrl({'maxWidth': photos[i].width})
        }

        return photosArr;
    }

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: PYRMONT,
            zoom: ZOOM
        });
    }

    var updateUrl = $('input[name="updateUrl"]').val();

    function updateInstitution(services, i)
    {
        var service = new google.maps.places.PlacesService(map);
        service.getDetails({
            placeId: services[i].service_id
        }, function(result, status) {
            if (status == 'OK') {
                var allday = false;
                if (result.opening_hours && result.opening_hours.periods) {
                    for (var j=0;j<=result.opening_hours.periods.length-1;j++) {
                        if($.inArray("close",Object.keys(result.opening_hours.periods[j])) && (
                            result.opening_hours.periods[j]['open'].day==0 || result.opening_hours.periods[j]['open'].time=="0000")
                        ) {
                            allday = true;
                        }
                    }
                }

                var photo = createPhotoMarker(result);

                $.ajax({
                    url: updateUrl,
                    type: 'post',
                    data: {
                        name: result.name,
                        categories: result.types,
                        address: result.formatted_address, // The formatted_address property is only returned for a Text Search
                        phone: result.international_phone_number,
                        site: result.website,
                        lat: result.geometry.location.lat(),
                        lng: result.geometry.location.lng(),
                        service_id: result.place_id,
                        all_day: (result.opening_hours && result.opening_hours.periods && allday) ? 1 : 0,
                        working_hours: (result.opening_hours) ? JSON.stringify(result.opening_hours) : '',
                        photos: photo
                    },
                    success: function () {
                        $('.objectsCount').text($('.objectsCount').text()*1 + 1);
                    }
                }).done(function () {
                    if (i < (services.length - 1) ) {
                        setTimeout(function () {
                            updateInstitution(services, ++i);
                        }, 100);
                    }
                });

            } else {
                console.log(status);
                location.href = location.origin + location.pathname + '?number='+i;
            }

        });
    }
}

$(document).ready(function () {
    if ($('.importMap').length) {
        var getStart = parseInt(getParameterByName('from'));
        var getEnd = parseInt(getParameterByName('to'));
        console.log(getStart, getEnd);
        if (getStart >= 0 && getEnd >= 0 && getEnd > getStart) {
            console.log('restart runInit(' + getStart + ',' + getEnd + ')');
            runInit(getStart, getEnd);
        }
    }
    if ($('.updateMap').length) {
        var getNumber = parseInt(getParameterByName('number'));
        if (getNumber >= 0) {
            updateInstitution(services_id, getNumber);
        } else {
            updateInstitution(services_id, 0);
        }
    }
});

