/**
 * Created by taras on 06.02.16.
 */
var config      = require('../config/config');
var mongoose    = require('mongoose');
var winston = require('winston');
//console.log(config.get('mongoose:uri'));
mongoose.connect(config.get('mongoose:uri'));
var db = mongoose.connection;

db.on('error', function (error) {
    winston.error('connection error:', error.message);
});
db.once('open', function callback () {
    winston.info("Connected to DB!");
});

var Schema = mongoose.Schema;

var Users = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true }
});

var UsersModel = mongoose.model('Users', Users);

module.exports.UsersModel = UsersModel;