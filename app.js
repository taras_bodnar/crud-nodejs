/**
 * Created by taras on 06.02.16.
 */
var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var config = require('./config/config');


app.use("/public", express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.get('/', routes.list);
app.get('/institution/:id', routes.getInstitution);
app.post('/users', routes.insert);
app.get('/user/:id', routes.edit);
app.post('/user/:id', routes.save);
app.put('/user/:id', routes.save);
app.delete('/delete/:id', routes.delete);

//var server = app.listen(config.get('port'), function() {
//    console.log('Express server listening on port ' + config.get('port'));
//});

http.createServer(app).listen(config.get('port'), function(){
    console.log('Express server listening on port ' + config.get('port'));
});