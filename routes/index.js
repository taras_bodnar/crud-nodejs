/**
 * Created by taras on 06.02.16.
 */
var winston = require('winston');
var express = require('express');
//var UsersModel    = require('../vendor/mongoose').UsersModel;
var connection    = require('../vendor/mysql');
var app = express();
exports.list = function(request, response,next){
    connection.query('SELECT * from institution limit 5000', function(err, rows, fields) {
        app.on('close', function() {
            connection.end();
        });

        if (!err) {
            //console.log(rows);
            response.render("list", { layout: false, items:JSON.stringify(rows),zoom:5});
            //return response.render('list',{items:rows });
        }  else {
            console.log('Error while performing Query.');
        }
    });
};


exports.getInstitution = function(request, response){
    connection.query('SELECT * from institution where id='+request.params.id+' limit 1', function(err, rows, fields) {
        app.on('close', function() {
            connection.end();
        });
        if (!err) {
            //console.log(rows);
            return response.render('list',{items:JSON.stringify(rows) });
        }  else {
            console.log('Error while performing Query.');
        }
    });

};

exports.insert = function(request,response) {
    var user = new UsersModel({
        name:  request.body.name,
        email: request.body.email,
        password:  request.body.password
    });
    user.save(function (error) {
        if (!error) {
            return response.send({status:'OK',user:user});
        } else {
            console.log(error);
            if(error.name == 'ValidationError') {
                response.statusCode = 400;
                response.send({ error: 'Validation error' });
            } else {
                res.statusCode = 500;
                response.send({ error: 'Server error' });
            }
            winston.error('Internal error(%d): %s',response.statusCode,error.message);
        }
    });
};

exports.edit = function(request, response){
    return UsersModel.findById(request.params.id, function (err, user) {
        if(!user) {
            response.statusCode = 404;
            return response.send({ error: 'User Not Found' });
        }
        if (!err) {
            return response.render('add',{user:user,action:'/user/'+user.id });
            //return response.send({status:'OK',user:user});
        } else {
            res.statusCode = 500;
            winston.error('Internal error(%d): %s',response.statusCode,err.message);
            return response.send({ error: 'Server error' });
        }
    });
};

exports.save = function(request, response){
    return UsersModel.findById(request.params.id, function (err, user) {
        if(!user) {
            response.statusCode = 404;
            return response.send({ error: 'User Not Found' });
        }
        user.name = request.body.name;
        user.email = request.body.email;
        user.password = request.body.password;

        return user.save(function (err) {
            if (!err) {
                return response.send({status:'OK',user:user});
            } else {
                response.statusCode = 500;
                winston.error('Internal error(%d): %s',response.statusCode,err.message);
                return response.send({ error: 'Server error' });
            }
        });
    });
};

exports.delete = function(request, response){
    return UsersModel.findById(request.params.id, function (err, user) {
        if(!user) {
            response.statusCode = 404;
            return response.send({ error: 'User Not Found' });
        }
        return user.remove(function (err) {
            if (!err) {
                return response.send({status:'OK'});
            } else {
                res.statusCode = 500;
                winston.error('Internal error(%d): %s',response.statusCode,err.message);
                return response.send({ error: 'Server error' });
            }
        });
    });
};